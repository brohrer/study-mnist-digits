from cottonwood.data.mnist import TestingData
from cottonwood.loggers import ConfusionLogger
from cottonwood.structure import load_structure

learning_rate = 1e-3
kernel_size = 3
l1_param = 1e-4
l2_param = 1e-5
n_classes = 10
n_kernels = 16
n_testing_iterations = 1e4

testing_data = TestingData()
model_filename = "mnist_classifier.pkl"

classifier = load_structure(model_filename)
classifier.add(testing_data, "testing_data")
classifier.connect("testing_data", "conv_2D_0", i_port_tail=0)
classifier.connect("testing_data", "one_hot", i_port_tail=1)

total_loss = 0
confusion_matrix = ConfusionLogger(n_iter_report=n_testing_iterations - 1)
for i_iter in range(int(n_testing_iterations)):
    classifier.forward_pass()
    total_loss += classifier.blocks["loss"].loss
    confusion_matrix.log_values(
      classifier.blocks["hard_max"].result,
      classifier.blocks["one_hot"].result,
      labels=classifier.blocks["one_hot"].get_labels())

print(f"loss: {total_loss / n_testing_iterations}")
print(f"accuracy: {confusion_matrix.calculate_accuracy()}")
