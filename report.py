import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
from cottonwood.data.mnist import TestingData
import cottonwood.experimental.visualize_structure as structure_viz
from cottonwood.structure import load_structure
import cottonwood.toolbox as tb

plt.switch_backend("agg")

n_render_examples = 36
n_plots = int(np.ceil(n_render_examples / 6))
n_render_examples = n_plots * 6

fig_height = 9
fig_width = 16

base_x = 1.5
spacing_x = 5
anchors_x = list((base_x + spacing_x * np.arange(3)) / fig_width)
base_y = 1.5
spacing_y = 4
anchors_y = list((base_y + spacing_y * np.arange(2)) / fig_height)
image_width = 2.5 / fig_width
image_height = 2.5 / fig_height
hist_width = image_width / 3
hist_spacing = hist_width / 2
dpi = 100

bg_color = "ivory"

model_filename = "mnist_classifier.pkl"
report_dir = os.path.join("reports", "examples")
os.makedirs(report_dir, exist_ok=True)


def main():
    classifier = load_structure(model_filename)
    tb.summarize(classifier)
    render_results(classifier)
    structure_viz.render(classifier)


def render_results(classifier):
    classifier.add(TestingData(), "testing_data")
    classifier.connect("testing_data", "conv_2D_0", i_port_tail=0)
    classifier.connect("testing_data", "one_hot", i_port_tail=1)
    right = []
    wrong = []

    # Gather a sampling of correct and incorrect examples to report
    while(True):
        classifier.forward_pass()
        image = classifier.blocks["testing_data"].image
        label = classifier.blocks["testing_data"].label
        prediction_array = classifier.blocks["logistic_2"].result
        label_names = classifier.blocks["one_hot"].get_labels()
        label_array = classifier.blocks["one_hot"].result
        top_choice = classifier.blocks["hard_max"].result
        predicted_label = label_names[np.where(top_choice)[0][0]]

        ordered_predictions = np.zeros(10)
        for label_name in range(10):
            i_label = label_names.index(label_name)
            ordered_predictions[label_name] = prediction_array[i_label]

        outcome = (image, label, predicted_label, ordered_predictions)
        if np.sum(np.abs(label_array - top_choice)) < 1e-3:
            if len(right) < n_render_examples:
                right.append(outcome)
        else:
            if len(wrong) < n_render_examples:
                wrong.append(outcome)

        if len(right) >= n_render_examples and len(wrong) >= n_render_examples:
            break
    assert(len(right) == n_render_examples)
    assert(len(wrong) == n_render_examples)

    plot_examples(right, label="correct")
    plot_examples(wrong, label="incorrect")


def plot_examples(examples, label=None):
    i_example = 0
    for i_plot in range(n_plots):
        fig_filename = f"{label}_{int(100 + i_plot)}.png"
        fig = plt.figure(figsize=(fig_width, fig_height), facecolor=bg_color)
        for anchor_x in anchors_x:
            for anchor_y in anchors_y:
                add_example(fig, examples[i_example], anchor_x, anchor_y)
                i_example += 1
        plt.savefig(
            os.path.join("reports", "examples", fig_filename),
            facecolor=fig.get_facecolor(),
            dpi=dpi)
        plt.close()


def add_example(fig, example, anchor_x, anchor_y):
    image, label, predicted_label, ordered_predictions = example
    ax_im = fig.add_axes((anchor_x, anchor_y, image_width, image_height))
    strip(ax_im)
    ax_im.imshow(
        image[:, :, 0],
        cmap="gray")
    ax_im.text(
        26, 30,
        f"predicted: {predicted_label}    actual: {label}",
        horizontalalignment="right",
        verticalalignment="center",
        fontsize=14,
    )
    ax_hist = fig.add_axes((
        anchor_x + image_width + hist_spacing,
        anchor_y,
        hist_width,
        image_height))
    strip(ax_hist)
    ax_hist.set_xlim(0, 1)
    ax_hist.set_ylim(0, 10)
    ax_hist.set_facecolor(bg_color)
    for i_label, prediction in enumerate(ordered_predictions):
        if prediction < .01:
            continue
        path = [
            [0, i_label],
            [0, i_label + 1],
            [prediction, i_label + 1],
            [prediction, i_label],
        ]
        if i_label == label:
            color = "forestgreen"
        else:
            color = "mediumblue"

        ax_hist.add_patch(patches.Polygon(
            path,
            facecolor=color,
            edgecolor="midnightblue",
            linewidth=.5))
        ax_hist.text(
            -.1, i_label + .5,
            str(i_label),
            horizontalalignment="right",
            verticalalignment="center")
    ax_hist.plot([0, 0], [0, 10], linewidth=.5, color="midnightblue")


def strip(ax):
    ax.spines["top"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.tick_params(bottom=False, top=False, left=False, right=False)
    ax.tick_params(
        labelbottom=False, labeltop=False, labelleft=False, labelright=False)


if __name__ == "__main__":
    main()
