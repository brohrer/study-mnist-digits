# Case Study: Classifying MNIST digits

This is a working example of image classification on the MNIST digits data set.
Compared to other image datasets, it's a pretty easy one to learn.
It's a good starting obstacle course for any model architecture
seeking to prove itself. 

## Get it running on your machine

This study is designed to run entirely on a desktop or laptop.
It doesn't use GPUs or clusters of CPUs.
You'll need to have Python 3 (including pip) installed.
There are a couple of other packages
you'll need to have in place to make it work.

### Cottonwood

The classifier model is built using [Cottonwood](https://e2eml.school/cottonwood),
a lightweight machine learning framework. 
Cottonwood changes quickly, so make sure
you have the right version. This study was built with version 28.
To do this at the command line:

    git clone https://gitlab.com/brohrer/cottonwood.git
    python3 -m pip install -e cottonwood
    cd cottonwood
    git checkout v28
    cd ..

Installing Cottonwood will also install its dependencies if you don't have
them already, [Numpy](https://numpy.org/),
[Numba](https://numba.pydata.org/), and
[Matplotlib](https://matplotlib.org/).

If you are new to Cottonwood, there's a pair of video walkthroughs of
the project [here](https://end-to-end-machine-learning.teachable.com/courses/322-convolutional-neural-networks-in-two-dimensions/lectures/24071959)
and [here](https://end-to-end-machine-learning.teachable.com/courses/322-convolutional-neural-networks-in-two-dimensions/lectures/24080930).

### MNIST digits data set

Thanks to @datapythonista, there is a convenient Python library
for downloading and using
[the MNIST digits data set](https://github.com/datapythonista/mnist).

It's included with Cottonwood and exposed in a ready-to-use
data block.

### Download the model

    git clone https://gitlab.com/brohrer/study-mnist-digits.git
    cd study-mnist-digits

### Train the model

    python3 train.py

### Test the model

    python3 test.py

For hints on how to make and test changes, check out
[Course 321](https://e2eml.school/321).


## The data

MNIST is a collection of handwritten digits, captured as 28 x 28 pixel
grayscale images.
You can find a lot more detail on them
[here](http://yann.lecun.com/exdb/mnist/).

## The model

The classifier used here is the convolutional neural network pictured below. 

![Model block diagram](reports/structure_diagram.png)

It has two convolutional layers and a linear layer. Most of the
activation functions are hyperbolic tangents, except for the final one,
which is a logistic function. This repository comes with a pre-trained
model in mnist_classifier.pkl.

The model performance is captured in the confusion matrix.

![Confusion matrix](reports/confusion_matrix.png)

There is also a handy visualizer for picking through correct and incorrect
predictions.

    python3 report.py

![Correct examples](reports/correct_examples.png)

![Incorrect examples](reports/incorrect_examples.png)
